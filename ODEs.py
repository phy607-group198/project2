"""
This module contains the class that defines the ordinary differential equations for a double pendulum
"""

import numpy as np

class ODE:
    """
    Class defining the ordinary differential equation

    Requires the masses and lengths of the two coupled pendulums and the acceleration due to gravity.

    Attributes
    ----------
    M1 : float
      mass of the first bob in kgs
    M2 : float
      mass of the second bob in kgs
    L1 : float
      length of the first pendulum
    L1 : float
      length of the second pendulum
    g : float
      acceleration due to gravity


    Methods
    -------
    omega1dot(self, omega1, omega2, theta1 , theta2)
      Returns the equation for the first derivative of the angular velocity of the first bob with respect to time
    omega2dot(self, omega1, omega2, theta1 , theta2)
      Returns the equation for the first derivative of the angular velocity of the second bob with respect to time
    theta1dot(self, omega1)
      Returns the angular velocity of the first bob
    theta2dot(self, omega2)
      Returns the angular velocity of the second bob
    grouped(self, t, var)
      Groups the four above functions into one function to pass to solve_ivp
    """

    def __init__(self, M1, M2, L1, L2, g):
        ## save the attributes
        self.M1 = M1
        self.M2 = M2
        self.L1 = L1
        self.L2 = L2
        self.g = g

    def omega1dot(self, omega1, omega2, theta1 , theta2):
        """
        First derivative of the angular velocity of the first bob with respect to time
        """
        return (-self.g*(2*self.M1+self.M2)*np.sin(theta1)-self.M2*np.sin(theta1-2*theta2)-2*np.sin(theta1-theta2)*self.M2*(self.L2*omega2**2+self.L1*omega1**2*np.cos(theta1-theta2)))/(self.L1*(2*self.M1+self.M2-self.M2*np.cos(2*theta1-2*theta2)))

    def omega2dot(self, omega1, omega2, theta1 , theta2):
        """
        First derivative of the angular velocity of the second bob with respect to time
        """
        return (2*np.sin(theta1-theta2)*(omega1**2*self.L1*(self.M1+self.M2)+self.g*(self.M1+self.M2)*np.cos(theta1)+omega2**2*self.L2*self.M2*np.cos(theta1-theta2)))/(self.L2*(2*self.M1+self.M2-self.M2*np.cos(2*theta1-2*theta2)))

    def theta1dot(self, omega1):
        """
        First derivative of the angular position of the first bob with respect to time, or just its angular velocity
        """
        return omega1

    def theta2dot(self, omega2):
        """
        First derivative of the angular position of the second bob with respect to time, or just its angular velocity
        """
        return omega2

    def grouped(self, t, var):
        """
        All four differential equations grouped into one 2d array
        """
        theta1 , omega1, theta2, omega2 = var
    
        theta1dot_var = self.theta1dot(omega1)
        theta2dot_var = self.theta2dot(omega2)
    
        omega1dot_var = self.omega1dot(omega1, omega2, theta1 , theta2)
        omega2dot_var = self.omega2dot(omega1, omega2, theta1 , theta2)
    
        result=[theta1dot_var, omega1dot_var, theta2dot_var, omega2dot_var]
    
        return np.array(result)

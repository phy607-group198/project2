##  Project 2
##  Anthony Estrada, Manabputra, Subham Vidyant
##  Start date:         13th Sept, 2022
##  Submission date:    7th Oct, 2022

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import animation
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.animation import PillowWriter
from scipy.integrate import solve_ivp

##  import functions and classes from other files
from ODEs import ODE
from algorithms import euler, rk4

##  function to convert angle in radians to a generalized angle in the range (-pi, pi)
def general_angle(theta):
    general_theta = theta % (2*np.pi)
    if (general_theta > np.pi ):
        return general_theta - (2*np.pi)
    else:
        return general_theta

##  function to do the generalized angle conversion to every element of a 2d array of angles in radians    
def general_angle_array(theta):
    for i in range(len(theta)):
        for j in range(len(theta[0])):
            theta[i][j] = general_angle(theta[i][j])
    return theta 

##  user inputs for properties of the double pendulum
M1= float(input(" \n Enter the mass of the 1st bob (in kgs) M1 (suggested: 1) : "))
M2= float(input(" \n Enter the mass of the 2nd bob (in kgs) M2 (suggested: 1) : "))
L1= float(input(" \n Enter the length of the 1st rod (in mtrs) L1 (suggested: 1) : "))
L2= float(input(" \n Enter the length of the 2nd rod (in mtrs) L2 (suggested: 1) : "))

omega1_0=float(input(" \n Enter initial angular velocity of the 1st bob (in rads/sec) ω_1 (suggested: 5) : "))
omega2_0=float(input(" \n Enter initial angular velocity of the 2nd bob (in rads/sec) ω_2 (suggested: -5) : "))
theta1_0=float(input(" \n Enter initial angular position of the 1st bob (in degrees) θ_1  (suggested: 50) : "))*np.pi/180   #convert degrees into radians
theta2_0=float(input(" \n Enter initial angular position of the 2nd bob (in degrees) θ_2  (suggested: 80) : "))*np.pi/180  #convert degrees into radians

##  total time the analysis runs for
time_count= int(input(" \n Enter the total time of simulation (in secs) (suggested: 15) : "))
##  number of points in the numerical analysis (scaled to 50 times the total time to keep animation at 50fps and close to real time)
d_step = time_count*50
##  step size in seconds
d_time=time_count/d_step
##  time array
time= np.linspace(0,time_count,d_step)

##  acceleration due to gravity on Earth. Change to 1.63 to simulate moon gravity
g = 9.81

##  algorithm selection
type = int(input(" \n Enter the algorithm to use Euler(1) or RK4(2) or SciPy(3) : "))

##  this is the error introduced into the starting conditions. The analysis is run 5 times on -2, -1, +1, and +2 of this delta on the initial thetas and omegas
delta = float(input(" \n Enter the error in theta's and omega's (Suggested: 0.001) : "))

##  initializing arrays that hold the theta and omega values. size is the number of points determined earlier
omega1=np.zeros(d_step)
omega2=np.zeros(d_step)
theta1=np.zeros(d_step)
theta2=np.zeros(d_step)

##  this is just a 2d array of all the thetas and omegas together to pass into solve_ivp
var = np.array([omega1, omega2, theta1, theta2])

##  changing the initial values of thetas and omegas to the user input -2*delta for start
omega1[0]=omega1_0 - 2*delta
omega2[0]=omega2_0 - 2*delta
theta1[0]=theta1_0 - 2*delta
theta2[0]=theta2_0 - 2*delta

##  (5 x d_step) solution array to hold all the variables from the 5 runs in one 2d array
omega1_sol=np.zeros((5,d_step))
omega2_sol=np.zeros((5,d_step))
theta1_sol=np.zeros((5,d_step))
theta2_sol=np.zeros((5,d_step))

##  (x,y) coordinates of the first and second bob of the pendulum
x1=np.zeros((5,d_step))
x2=np.zeros((5,d_step))
y1=np.zeros((5,d_step))
y2=np.zeros((5,d_step))

##  velocities of the bobs
x1dot=np.zeros((5,d_step))
x2dot=np.zeros((5,d_step))
y1dot=np.zeros((5,d_step))
y2dot=np.zeros((5,d_step))

##  create an instance of the class ODE with the user input parameters
ode = ODE(M1, M2, L1, L2, g)

##  loop to run the analysis 5 times, on -2, -1, 0, +1, and +2 delta on the starting conditions
for i in range(5):
    ##  run euler's method
    if type == 1:
        ##  variable used to name files later on
        solmet = 'euler'
        euler_sol = euler(ode, omega1, omega2, theta1, theta2, d_step, d_time)
        ##  save the solution into the i'th row of the solution array for the i'th run
        omega1_sol[i], omega2_sol[i], theta1_sol[i], theta2_sol[i] = euler_sol

    ##  run RK4 method
    if type ==2:
        solmet = 'RK4'
        rk4_sol = rk4(ode, omega1, omega2, theta1, theta2, d_step, d_time)
        omega1_sol[i], omega2_sol[i], theta1_sol[i], theta2_sol[i] = rk4_sol

    ##  run scipy's implimentation of solve_ivp, defaulted to the RK45 method
    if type == 3:
        solmet = 'scipy'
        scipy_sol= solve_ivp(ode.grouped,(0,time_count),(theta1[0],omega1[0],theta2[0],omega2[0]),t_eval=time)
        theta1_sol[i], omega1_sol[i], theta2_sol[i], omega2_sol[i] = scipy_sol.y
    
    ##  increment the starting conditions for the next run
    omega1[0]=omega1[0]+delta
    omega2[0]=omega2[0]+delta
    theta1[0]=theta1[0]+delta
    theta2[0]=theta2[0]+delta

    ##  calculating the position coordinates of the bobs in terms of thetas
    x1[i]=L1*np.sin(theta1_sol[i])
    x2[i]=x1[i]+L2*np.sin(theta2_sol[i])
    y1[i]=-L1*np.cos(theta1_sol[i])
    y2[i]=y1[i]-L2*np.cos(theta2_sol[i])

    ##  calculating the velocities of the bobs in terms of thetas and omegas
    x1dot[i]=L1*omega1_sol[i]*np.cos(theta1_sol[i])
    x2dot[i]=x1dot[i]+L2*omega2_sol[i]*np.cos(theta2_sol[i])
    y1dot[i]=L1*omega1_sol[i]*np.sin(theta1_sol[i])
    y2dot[i]=y1dot[i]+L2*omega2_sol[i]*np.sin(theta2_sol[i])

##  function to create the lines for the pendulum and traces in the gif
def animate(i):
    ln10.set_data([x2[4][i-20:i]],[y2[4][i-20:i]])
    ln9.set_data([0,x1[4][i],x2[4][i]],[0,y1[4][i],y2[4][i]])
    ln8.set_data([x2[3][i-20:i]],[y2[3][i-20:i]])
    ln7.set_data([0,x1[3][i],x2[3][i]],[0,y1[3][i],y2[3][i]])
    ln6.set_data([x2[2][i-20:i]],[y2[2][i-20:i]])
    ln5.set_data([0,x1[2][i],x2[2][i]],[0,y1[2][i],y2[2][i]])
    ln4.set_data([x2[1][i-20:i]],[y2[1][i-20:i]])
    ln3.set_data([0,x1[1][i],x2[1][i]],[0,y1[1][i],y2[1][i]])
    ln2.set_data([x2[0][i-20:i]],[y2[0][i-20:i]])
    ln1.set_data([0,x1[0][i],x2[0][i]],[0,y1[0][i],y2[0][i]])
    return ln1,ln2,ln3,ln4,ln5,ln6,ln7,ln8,ln9,ln10

print(" \n Please wait for the animation to render and look for the '{}.gif' file".format(solmet))

##  create and save the gif file on the stacked swinging pendulums
fig, ax= plt.subplots(1,1, figsize=(8,8))
ax.set_facecolor('k')
ln10, =plt.plot([],[],'m', lw=3, markersize=1)
ln9, =plt.plot([],[],'ro--', lw=2, markersize=5)
ln8, =plt.plot([],[],'c', lw=3, markersize=1)
ln7, =plt.plot([],[],'ro--', lw=2, markersize=5)
ln6, =plt.plot([],[],'y', lw=3, markersize=1)
ln5, =plt.plot([],[],'ro--', lw=2, markersize=5)
ln4, =plt.plot([],[],'g', lw=3, markersize=1)
ln3, =plt.plot([],[],'ro--', lw=2, markersize=5)
ln2, =plt.plot([],[],'b', lw=3, markersize=1)
ln1, =plt.plot([],[],'ro--', lw=2, markersize=5)
ax.get_xaxis().set_ticks([])
ax.get_yaxis().set_ticks([])
ax.set_ylim(-(1.2*L1+L2),(1.2*L1+L2))
ax.set_xlim(-(1.2*L1+L2),(1.2*L1+L2))
ani=animation.FuncAnimation(fig, animate, frames=d_step,interval=0)
ani.save('{}.gif'.format(solmet), writer='pillow', fps=d_step/time_count)
plt.show()

print(" \n Please wait for the plot to render and look for the 'divergence.png' file")

##  convert thetas into generalized angle
theta1_sol_gen=general_angle_array(theta1_sol)
theta2_sol_gen=general_angle_array(theta2_sol)

##  calculate the divergence between the runs with errors (i = 0,1,3,4) and the run without error (i = 2)
omega1diff1 = omega1_sol[2]-omega1_sol[0]
omega2diff1 = omega2_sol[2]-omega2_sol[0]
theta1diff1 = theta1_sol[2]-theta1_sol[0]
theta2diff1 = theta2_sol[2]-theta2_sol[0]

omega1diff2 = omega1_sol[2]-omega1_sol[1]
omega2diff2 = omega2_sol[2]-omega2_sol[1]
theta1diff2 = theta1_sol[2]-theta1_sol[1]
theta2diff2 = theta2_sol[2]-theta2_sol[1]

omega1diff3 = omega1_sol[2]-omega1_sol[3]
omega2diff3 = omega2_sol[2]-omega2_sol[3]
theta1diff3 = theta1_sol[2]-theta1_sol[3]
theta2diff3 = theta2_sol[2]-theta2_sol[3]

omega1diff4 = omega1_sol[2]-omega1_sol[4]
omega2diff4 = omega2_sol[2]-omega2_sol[4]
theta1diff4 = theta1_sol[2]-theta1_sol[4]
theta2diff4 = theta2_sol[2]-theta2_sol[4]

##  create and save the png file for divergence in the thetas and omegas
fig, axs = plt.subplots(2, 2)
fig.suptitle('Divergence due to chaos')
axs[0, 0].plot(time, omega1diff1, 'tab:blue', label='-2$\delta$')
axs[0, 0].plot(time, omega1diff2, 'tab:orange', label='-$\delta$')
axs[0, 0].plot(time, omega1diff3, 'tab:green', label='$\delta$')
axs[0, 0].plot(time, omega1diff4, 'tab:red', label='2$\delta$')
axs[0, 0].set_title('$\omega_1$')
axs[0, 1].plot(time, omega2diff1, 'tab:blue', label='-2$\delta$')
axs[0, 1].plot(time, omega2diff2, 'tab:orange', label='-$\delta$')
axs[0, 1].plot(time, omega2diff3, 'tab:green', label='$\delta$')
axs[0, 1].plot(time, omega2diff4, 'tab:red', label='2$\delta$')
axs[0, 1].set_title('$\omega_2$')
axs[1, 0].plot(time, theta1diff1, 'tab:blue', label='-2$\delta$')
axs[1, 0].plot(time, theta1diff2, 'tab:orange', label='-$\delta$')
axs[1, 0].plot(time, theta1diff3, 'tab:green', label='$\delta$')
axs[1, 0].plot(time, theta1diff4, 'tab:red', label='2$\delta$')
axs[1, 0].set_title('$\\theta_1$')
axs[1, 1].plot(time, theta2diff1, 'tab:blue', label='-2$\delta$')
axs[1, 1].plot(time, theta2diff2, 'tab:orange', label='-$\delta$')
axs[1, 1].plot(time, theta2diff3, 'tab:green', label='$\delta$')
axs[1, 1].plot(time, theta2diff4, 'tab:red', label='2$\delta$')
axs[1, 1].set_title('$\\theta_2$')

for ax in axs.flat:
    ax.set(xlabel='Time', ylabel='Divergence')

##  hide x labels and tick labels for top plots and y ticks for right plots
for ax in axs.flat:
    ax.label_outer()
    ax.legend()
fig.savefig('divergence.png')
plt.show()

print(" \n Please wait for the plot to render and look for the 'phase_space.png' file")

##  plot the phase space diagrams
fig, axs2 = plt.subplots(1, 2,figsize=(10,4))
fig.suptitle('Phase space diagrams')
axs2[0].scatter(x=theta1_sol_gen[2], y=omega1_sol[2],color='blue')
axs2[0].set_title('$\omega_1$ vs $\\theta_1$')
axs2[1].scatter(x=theta2_sol_gen[2],y= omega2_sol[2],color='green')
axs2[1].set_title('$\omega_2$ vs $\\theta_2$')
axs2[0].set_xlabel('$\\theta_1$')
axs2[1].set_xlabel('$\\theta_2$')
axs2[0].set_ylabel('$\omega_1$')
axs2[1].set_ylabel('$\omega_2$')
fig.savefig('phase_space.png')
plt.show()

print(" \n Please wait for the plot to render and look for the 'correlation.png' file")

##  initialize arrays to store the "correlation"
corr1 = np.zeros(d_step)
corr2 = np.zeros(d_step)
corr3 = np.zeros(d_step)
corr4 = np.zeros(d_step)
corr5 = np.zeros(d_step)
corr6 = np.zeros(d_step)
corr7 = np.zeros(d_step)
corr8 = np.zeros(d_step)

##  calculate the correlation
for i in range(1,d_step+1):
    mul1=np.zeros(i)
    mul2=np.zeros(i)
    mul3=np.zeros(i)
    mul4=np.zeros(i)
    mul5=np.zeros(i)
    mul6=np.zeros(i)
    mul7=np.zeros(i)
    mul8=np.zeros(i)
    for j in range(i):
        mul1[j] = (x2[0][j]*x2[2][j])
        mul2[j] = (x2[1][j]*x2[2][j])
        mul3[j] = (x2[3][j]*x2[2][j])
        mul4[j] = (x2[4][j]*x2[2][j])
        mul5[j] = (y2[0][j]*y2[2][j])
        mul6[j] = (y2[1][j]*y2[2][j])
        mul7[j] = (y2[3][j]*y2[2][j])
        mul8[j] = (y2[4][j]*y2[2][j])
    corr1[i-1] = sum(mul1)/i
    corr2[i-1] = sum(mul2)/i
    corr3[i-1] = sum(mul3)/i
    corr4[i-1] = sum(mul4)/i
    corr5[i-1] = sum(mul5)/i
    corr6[i-1] = sum(mul6)/i
    corr7[i-1] = sum(mul7)/i
    corr8[i-1] = sum(mul8)/i

##  plot the correlation
fig, axs3 = plt.subplots(1, 2,figsize=(10,4))
fig.suptitle('Correlation function')
axs3[0].plot(corr1, 'tab:blue')
axs3[0].plot(corr2, 'tab:red')
axs3[0].plot(corr3, 'tab:green')
axs3[0].plot(corr4, 'tab:orange')
axs3[0].set_title('X Correlation')
axs3[1].plot(corr5, 'tab:blue')
axs3[1].plot(corr6, 'tab:red')
axs3[1].plot(corr7, 'tab:green')
axs3[1].plot(corr8, 'tab:orange')
axs3[1].set_title('Y Correlation')
axs3[0].set_ylabel('$\\langle x_2.x_2\\rangle$')
axs3[1].set_ylabel('$\\langle y_2.y_2\\rangle$')
axs3[0].set_xlabel('Time step')
axs3[1].set_xlabel('Time step')
fig.savefig('correlation.png')
plt.show()


##  root finding

##  copy over the anglular position of the second bob in generalized angle
ang_pos = theta2_sol_gen[2]
ang_pos_plus90 = ang_pos + (np.pi/2)
ang_pos_minus90 = ang_pos - (np.pi/2)

##  count keeping track of how many times the pendulum "flips"
count = 0

##  array storing the i values at which the pendulum flips
i_val = []

##  find how many times and when the pendulum flips to the left
for i in range(d_step-1):
    ##  check if ang_pos_plus90 flips sign
    if ang_pos_plus90[i]*ang_pos_plus90[i+1]<0:
        count = count + 1
        i_val.append(i)

##  find how many times and when the pendulum flips to the right
for i in range(d_step-1):
    ##  check if ang_pos_minus90 flips sign
    if ang_pos_minus90[i]*ang_pos_minus90[i+1]<0:
        count = count + 1
        i_val.append(i)

##  sort the i values in ascending order
i_val.sort()

##  array storing the time values when the pendulum flips
time_value = []

##  interpolate the time when the pendulum flips
for i in i_val:
    time_value.append((time[i]+time[i+1])/2)

print("\n The pendulum flips {} times at: \n" .format(count))
print(time_value)

print("\n That's the end. Hurray!!")
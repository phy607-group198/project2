# Project2

## Analysis of a double pendulum
Authors : Anthony Estrada, Manabputra, Subham Vidyant

## Description
We present a script that takes in user inputs to describe a double pendulum and its initial conditions, and then animates its behaviour in time using three different numerical analysis methods that the user can choose between, and presents data corresponding to the same. Using this script we can show how the double pendulum is a chaotic system.

## Features
The script asks the user for a float input that will be used as an "error" term. This term is used to offset the initial conditions that the user also provides, and generate 5 sets of new intial conditions that are -2, -1, 0, +1, and +2 times the error away from the original intial condition. The script then runs the simulation 5 times with these 5 slightly different intial conditions and compares them.

## GIF
The GIF outputted by the script using the recommended user inputs is shown below.

Euler's Method:

![](euler.gif)


RK4 Method:

![](RK4.gif)


scipy.solve_ivp implimentation of RK45 Method:

![](scipy.gif)


## Root Finding (Extra)
To ensure completeness of the project, we have implimented a root finding algorithm on the double pendulum. By finding when and how many times the second bob's angular position crosses 90 degrees and -90 degrees, we can find out when the pendulum "flips", that is when the second bob is above the first bob. The root finding algorithm works by looking for sign changes in angles, or simple functions thereof.

## Working and Usage
- The script can be run by running __main__.py. The dependancies are SciPy, matplotlib, numpy, and mpl_toolkits. 
- The user will have to first enter the masses and lengths, and then the initial conditions of the double pendulum.
- The script will then ask for the total time it should run the simulation for.
- It will then ask the user to choose the algorithm for the numerical analysis.
- Finally the user has to enter the "error" term.
- Suggested values of all of the above inputs are mentioned in the prompts.
- The script then does the numerical analysis and shows (and saves) a gif of the five pendulums described by the user and its motion for the determined amount of time. Close this window to proceed.
- The script then shows (and saves) a plot of the divergence in the thetas and omegas of the four pendulums with errors from the one without any error in the starting conditions. Close this window to proceed.
- The script then shows (and saves) the phase space diagram of the two pendulums. Close this window to proceed.
- The script then shows (and saves) a correlation function of sorts that shows the pendulums getting further apart as time progresses. Close this window to proceed.
- The script then does the root finding explained above and outputs the number of times and the timestamps of when the pendulum flips.

'''
This module has the different algorithms to do the numerical analysis.
All functions take an ODE class instance, all the thetas and omegas, the number of points in the analysis, and the time step size.

Available functions:
euler
rk4
'''

import numpy as np

def euler(ode, omega1, omega2, theta1, theta2, d_step, d_time):
    '''
    Returns the time progressed thetas and omegas by solving the ordinary differential equations in ODE using the Euler's method

    Parameters
    ----------
    ode : ODE
      class defining the ordinary differential equation
    omega1 : array
      array of angular velocities of the first bob
    omega2 : array
      array of angular velocities of the second bob
    theta1 : array
      array of angular position of the first bob
    theta2 : array
      array of angular position of the second bob
    d_step : float
      number of points in the analysis
    d_time : float
      time step size

    Returns
    -------
    A (4 x d_step) array containing the time progressed thetas and omegas 
    '''
    for i in range(1, d_step):
        ##  implimemtation of the euler's method
        omega1[i] = omega1[i-1] + (ode.omega1dot(omega1[i-1], omega2[i-1], theta1[i-1], theta2[i-1]))*d_time
        omega2[i] = omega2[i-1] + (ode.omega2dot(omega1[i-1], omega2[i-1], theta1[i-1], theta2[i-1]))*d_time
        theta1[i] = theta1[i-1] + ode.theta1dot(omega1[i-1])*d_time
        theta2[i] = theta2[i-1] + ode.theta1dot(omega2[i-1])*d_time
    result = [omega1, omega2, theta1, theta2]
    return np.array(result)


def rk4(ode, omega1, omega2, theta1, theta2, d_step, d_time):
    '''
    Returns the time progressed thetas and omegas by solving the ordinary differential equations in ODE using the RK4 method

    Parameters
    ----------
    ode : ODE
      class defining the ordinary differential equation
    omega1 : array
      array of angular velocities of the first bob
    omega2 : array
      array of angular velocities of the second bob
    theta1 : array
      array of angular position of the first bob
    theta2 : array
      array of angular position of the second bob
    d_step : float
      number of points in the analysis
    d_time : float
      time step size
    Returns
    -------
    A (4 x d_step) array containing the time progressed thetas and omegas 
    '''
    for i in range(1, d_step):
        ##  implimemtation of the rk4 method
        omega1_k1 = ode.omega1dot(omega1[i-1], omega2[i-1], theta1[i-1], theta2[i-1])
        omega2_k1 = ode.omega2dot(omega1[i-1], omega2[i-1], theta1[i-1], theta2[i-1])
        theta1_k1 = ode.theta1dot(omega1[i-1])
        theta2_k1 = ode.theta2dot(omega2[i-1])

        omega1_k2 = ode.omega1dot(omega1[i-1] + omega1_k1*d_time/2, omega2[i-1] + omega2_k1*d_time/2, theta1[i-1] + theta1_k1*d_time/2, theta2[i-1] + theta2_k1*d_time/2)
        omega2_k2 = ode.omega2dot(omega1[i-1] + omega1_k1*d_time/2, omega2[i-1] + omega2_k1*d_time/2, theta1[i-1] + theta1_k1*d_time/2, theta2[i-1] + theta2_k1*d_time/2)
        theta1_k2 = ode.theta1dot(omega1[i-1]+omega1_k1*d_time/2)
        theta2_k2 = ode.theta2dot(omega2[i-1]+omega2_k1*d_time/2)

        omega1_k3 = ode.omega1dot(omega1[i-1] + omega1_k2*d_time/2, omega2[i-1] + omega2_k2*d_time/2, theta1[i-1] + theta1_k2*d_time/2, theta2[i-1] + theta2_k2*d_time/2)
        omega2_k3 = ode.omega2dot(omega1[i-1] + omega1_k2*d_time/2, omega2[i-1] + omega2_k2*d_time/2, theta1[i-1] + theta1_k2*d_time/2, theta2[i-1] + theta2_k2*d_time/2)
        theta1_k3 = ode.theta1dot(omega1[i-1]+omega1_k2*d_time/2)
        theta2_k3 = ode.theta2dot(omega2[i-1]+omega2_k2*d_time/2)

        omega1_k4 = ode.omega1dot(omega1[i-1] + omega1_k3*d_time, omega2[i-1] + omega2_k3*d_time, theta1[i-1] + theta1_k3*d_time, theta2[i-1] + theta2_k3*d_time)
        omega2_k4 = ode.omega2dot(omega1[i-1] + omega1_k3*d_time, omega2[i-1] + omega2_k3*d_time, theta1[i-1] + theta1_k3*d_time, theta2[i-1] + theta2_k3*d_time)
        theta1_k4 = ode.theta1dot(omega1[i-1]+omega1_k3*d_time)
        theta2_k4 = ode.theta2dot(omega2[i-1]+omega2_k3*d_time)

        omega1[i] = omega1[i-1] + (omega1_k1+2*omega1_k2+2*omega1_k3+omega1_k4)*d_time/6
        omega2[i] = omega2[i-1] + (omega2_k1+2*omega2_k2+2*omega2_k3+omega2_k4)*d_time/6
        theta1[i] = theta1[i-1] + (theta1_k1+2*theta1_k2+2*theta1_k3+theta1_k4)*d_time/6
        theta2[i] = theta2[i-1] + (theta2_k1+2*theta2_k2+2*theta2_k3+theta2_k4)*d_time/6
    result = [omega1, omega2, theta1, theta2]
    return np.array(result)
        